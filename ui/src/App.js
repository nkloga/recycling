import React, { useState, useMemo } from "react";
import "./App.css";
import Landing from "./landing/Landing";
import Main from "./components/Main";
import { BrowserRouter as Router, Route } from "react-router-dom";
import UserContext from "./context/UserContext";
import AuthorizedRoute from "./components/AuthorizedRoute";

function AppRouter() {
  const [user, setUser] = useState(null);
  const value = useMemo(() => ({ user, setUser }), [user, setUser]);

  return (
    <Router>
      <UserContext.Provider value={value}>
        <AuthorizedRoute exact path="/main" component={Main} />
        <Route exact path="/" component={Landing} />
      </UserContext.Provider>
    </Router>
  );
}

export default AppRouter;
