import axios from "axios";
import Cookies from "js-cookie";

const baseUrl = process.env.REACT_APP_BASE_URL;
axios.defaults.headers.common["Content-Type"] = "application/json";

const request = async (method, path, data = null) => {
  const token = Cookies.get("token");

  if (token) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
  }

  const url = `${baseUrl}${path}`;
  const req = { url, method };
  if (method === "GET" && data) {
    req.params = data;
  } else if (data) {
    req.data = data;
  }
  return axios(req);
};

export const getBase = async path => axios.get(`${baseUrl}${path}`);

export const getApi = async (path, params = null) =>
  request("GET", path, params);
export const postApi = async (path, data = null) => request("POST", path, data);
export const putApi = async (path, data = null) => request("PUT", path, data);
export const patchApi = async (path, data = null) =>
  request("PATCH", path, data);
export const deleteApi = async (path, data = null) =>
  request("DELETE", path, data);

export default {
  getApi,
  postApi,
  putApi,
  patchApi,
  deleteApi
};
