import { postApi, getApi } from "./Requests";

export const auth = async (username, password) =>
  postApi("/auth", {
    username: username,
    password: password
  });

export const createGame = async (username, password) =>
  getApi("/game/create", { username, password });
