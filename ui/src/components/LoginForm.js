import React, { useContext, useState } from "react";
import UserContext from "../context/UserContext";
import { useHistory } from "react-router-dom";
import { auth } from "../api/GameApi";
import Cookie from "js-cookie";

export function LoginForm() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const { setUser } = useContext(UserContext);
  const history = useHistory();

  function handleSubmit(event) {
    event.preventDefault();
    auth(username, password)
      .then(response => {
        const token = response.data.token;
        const auth = response.data.auth;
        const username = JSON.parse(atob(token.split(".")[1]));
        Cookie.set("token", token);
        setUser({
          username: username,
          token: token,
          auth: auth,
          isLoggedIn: true
        });
        history.push("/main");
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  return (
    <form
      name="login"
      id="loginForm"
      onSubmit={handleSubmit}
      noValidate="novalidate"
    >
      <div className="control-group">
        <div className="form-group floating-label-form-group controls mb-0 pb-2">
          <label>Email Address</label>
          <input
            name="username"
            className="form-control"
            id="username"
            type="text"
            placeholder="User Name"
            value={username}
            onChange={e => setUsername(e.target.value)}
            required="required"
            data-validation-required-message="Please enter your user name."
          />
          <p className="help-block text-danger"></p>
        </div>
      </div>

      <div className="control-group">
        <div className="form-group floating-label-form-group controls mb-0 pb-2">
          <label>Password</label>
          <input
            className="form-control"
            name="password"
            id="password"
            type="password"
            placeholder="Password"
            value={password}
            onChange={e => setPassword(e.target.value)}
            required="required"
            data-validation-required-message="Please enter your password."
          />
          <p className="help-block text-danger"></p>
        </div>
      </div>
      <br />
      <div id="success"></div>
      <div className="form-row text-center">
        <div className="col-12">
          <button
            type="submit"
            className="btn btn-primary btn-xl"
            id="sendMessageButton "
          >
            Submit
          </button>
        </div>
        <div className="col-12">
          <br /> Don't have an account?{" "}
          <span>
            <a
              className=" py-3 px-0 px-lg-3 js-scroll-trigger"
              href="#register"
            >
              Sign up!
            </a>
          </span>
        </div>
      </div>
    </form>
  );
}
