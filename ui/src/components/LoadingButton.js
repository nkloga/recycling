import React, { useState, useEffect } from "react";
import Button from "react-bootstrap/Button";

export function LoadingButton({ text, api }) {
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    if (isLoading) {
      api().then(response => {
        setLoading(false);
        console.log(response);
      });
    }
  }, [isLoading, api]);

  const handleClick = () => setLoading(true);

  return (
    <Button
      variant="primary"
      disabled={isLoading}
      onClick={!isLoading ? handleClick : null}
    >
      {isLoading ? "Loading…" : text}
    </Button>
  );
}
