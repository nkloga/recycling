import React, { useContext } from "react";
import UserContext from "../context/UserContext";
import Nav from "./Nav";
import Footer from "./Footer";
import { LoadingButton } from "./LoadingButton";
import { createGame } from "../api/GameApi";

export default function Main() {
  const { user } = useContext(UserContext);

  return (
    <div>
      <Nav />
      <Menu user={user} />
      <Footer />
    </div>
  );
}

function Menu(props) {
  const auth = props.user.auth;
  const isLoggedIn = props.user.isLoggedIn;

  if (isLoggedIn && auth === "TEACHER") {
    return <TeacherMenu user={props.user} />;
  } else if (isLoggedIn && auth === "STUDENT") {
    return <StudentMenu user={props.user} />;
  }
}

function TeacherMenu(props) {
  const temp = {
    marginTop: "300px",
    marginBottom: "300px"
  };
  return (
    <div style={temp}>
      <LoadingButton text={"Start a game"} api={createGame} user={props.user} />
      <p>{JSON.stringify(props.user.auth)}</p>
    </div>
  );
}

function StudentMenu(props) {
  console.log(props);
  const temp = {
    marginTop: "300px",
    marginBottom: "300px"
  };
  return (
    <div style={temp}>
      <LoadingButton text={"Start a game"} />
      <p>{JSON.stringify(props.user.auth)}</p>
    </div>
  );
}
