import React, { useContext } from "react";
import { Route, Redirect } from "react-router-dom";
import UserContext from "../context/UserContext";

const AuthorizedRoute = props => {
  const user = useContext(UserContext);
  if (user.isLoggedIn === null) {
    return <Redirect to="/" />;
  }
  return <Route {...props} />;
};

export default AuthorizedRoute;
