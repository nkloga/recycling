import React from "react";
import { FaStar } from "react-icons/fa";
import { LoginForm } from "./LoginForm";

export default function Login() {
  return (
    <section className="page-section" id="login">
      <div className="container p-5">
        <h2 className="page-section-heading text-center text-uppercase text-secondary mb-0">
          Login
        </h2>
        <div className="divider-custom">
          <div className="divider-custom-line"></div>
          <div className="divider-custom-icon">
            <FaStar />
          </div>
          <div className="divider-custom-line"></div>
        </div>
        <div className="row">
          <div className="col-4 mx-auto  text-center">
            <LoginForm />
          </div>
        </div>
      </div>
    </section>
  );
}
