import React from "react";
import { FaFacebook, FaLinkedin, FaDribbble, FaTwitter } from "react-icons/fa";

const CopyR = () => {
  return (
    <div>
      <section className="copyright py-4 text-center text-white ">
        <div className="container ">
          <small>Copyright &copy; Recycling Website 2020</small>
        </div>
      </section>

      <div className="scroll-to-top d-lg-none position-fixed ">
        <a
          className="js-scroll-trigger d-block text-center text-white rounded "
          href="#page-top "
        >
          <i className="fa fa-chevron-up "></i>
        </a>
      </div>
    </div>
  );
};

export default function Footer() {
  return (
    <div>
      <footer className="footer text-center ">
        <div className="container ">
          <div className="row ">
            <div className="col-lg-4 mb-5 mb-lg-0 "></div>

            <div className="col-lg-4 mb-5 mb-lg-0 ">
              <h4 className="text-uppercase mb-4 ">Around the Web</h4>
              <a className="btn btn-outline-light btn-social mx-1 " href="# ">
                <FaFacebook />
              </a>
              <a className="btn btn-outline-light btn-social mx-1 " href="# ">
                <FaTwitter />
              </a>
              <a className="btn btn-outline-light btn-social mx-1 " href="# ">
                <FaLinkedin />
              </a>
              <a className="btn btn-outline-light btn-social mx-1 " href="# ">
                <FaDribbble />
              </a>
            </div>

            <div className="col-lg-4 "></div>
          </div>
        </div>
      </footer>
      <CopyR />
    </div>
  );
}
