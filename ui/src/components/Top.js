import React from "react";
import logo from "../assets/avataaars.svg";

export default function Top() {
  return (
    <div
      className="masthead bg-primary text-white text-center page-width"
      id="join"
    >
      <div className="container d-flex align-items-center flex-column">
        <img className="masthead-avatar mb-5" src={logo} alt="" />
        <h1 className="masthead-heading text-uppercase mb-0">
          Start Recycling
        </h1>

        <div className="divider-custom divider-light">
          <div className="divider-custom-line"></div>
          <div className="divider-custom-icon">
            <i className="fas fa-star"></i>
          </div>
          <div className="divider-custom-line"></div>
        </div>
        <p className="masthead-subheading font-weight-light mb-0">
          Test your knowlege - Know the impact - Save the planet
        </p>

        <br />
        <br />
        <form name="enterPin" id="contactForm" noValidate="novalidate">
          <div className="control-group p-5">
            <div className="form-group floating-label-form-group controls mb-0 pb-2">
              <input
                className="form-special"
                name="pin"
                id="pin"
                type="password"
                placeholder="Enter your PIN"
                required="required"
                data-validation-required-message="Please enter the correct PIN."
              />
              <p className="help-block text-danger"></p>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}
