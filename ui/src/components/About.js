import React from "react";
import { FaStar } from "react-icons/fa";

export default function About() {
  return (
    <section className="page-section bg-primary text-white mb-0 " id="about">
      <div className="container p-5">
        <h2 className="page-section-heading text-center text-uppercase text-white ">
          About
        </h2>

        <div className="divider-custom divider-light ">
          <div className="divider-custom-line "></div>
          <div className="divider-custom-icon ">
            <FaStar />
          </div>
          <div className="divider-custom-line "></div>
        </div>

        <div className="row ">
          <div className="col-lg-4 ml-auto ">
            <p className="lead ">
              Lorem ipsum dolor sit, amet consectetur adipisicing elit. Fugiat
              odio error est repellendus distinctio doloremque laborum tempora,
              nisi excepturi facere dignissimos voluptatum deleniti quos cum
              consectetur obcaecati, sapiente modi nam.
            </p>
          </div>
          <div className="col-lg-4 mr-auto ">
            <p className="lead ">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor
              consequuntur aperiam hic ea porro unde molestiae aut ad, in
              pariatur!
            </p>
          </div>
        </div>

        <div className="text-center mt-4 ">
          {/* <a className="btn btn-xl btn-outline-light " href="#">
              <i className="fas fa-download mr-2 "></i> get project materials!
            </a> */}
        </div>
      </div>
    </section>
  );
}
