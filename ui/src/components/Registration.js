import React from "react";
import { FaStar } from "react-icons/fa";

export default function Registration() {
  return (
    <section className="page-section" id="register">
      <div className="container ">
        <h2 className="page-section-heading text-center text-uppercase text-secondary mb-0 ">
          Register
        </h2>

        <div className="divider-custom ">
          <div className="divider-custom-line "></div>
          <div className="divider-custom-icon ">
            <FaStar />
          </div>
          <div className="divider-custom-line "></div>
        </div>

        <div className="row ">
          <div className="col-lg-8 mx-auto ">
            <form
              name="sentMessage "
              id="contactForm "
              noValidate="novalidate "
            >
              <div className="control-group ">
                <div className="form-group floating-label-form-group controls mb-0 pb-2 ">
                  <label>Name</label>
                  <input
                    className="form-control "
                    id="name "
                    type="text"
                    placeholder="Name "
                    required="required "
                    data-validation-required-message="Please enter your name. "
                  />
                  <p className="help-block text-danger "></p>
                </div>
              </div>
              <div className="control-group ">
                <div className="form-group floating-label-form-group controls mb-0 pb-2 ">
                  <label>Email Address</label>
                  <input
                    className="form-control "
                    id="email "
                    type="email"
                    placeholder="Email Address "
                    required="required "
                    data-validation-required-message="Please enter your email address. "
                  />
                  <p className="help-block text-danger "></p>
                </div>
              </div>
              <div className="control-group ">
                <div className="form-group floating-label-form-group controls mb-0 pb-2 ">
                  <label>Password</label>
                  <input
                    className="form-control "
                    id="password "
                    type="password"
                    placeholder="Password "
                    required="required "
                    data-validation-required-message="Please enter your password. "
                  />
                  <p className="help-block text-danger "></p>
                </div>
              </div>
              <div className="control-group ">
                <div className="form-group floating-label-form-group controls mb-0 pb-2 ">
                  <label>Password</label>
                  <input
                    className="form-control "
                    id="confirm-password "
                    type="password"
                    placeholder="Confirm Password "
                    required="required "
                    data-validation-required-message="Please confirm your password. "
                  />
                  <p className="help-block text-danger "></p>
                </div>
              </div>

              <br />
              <div id="success "></div>

              <div className="form-row text-center">
                <div className="col-12">
                  <button
                    type="submit"
                    className="btn btn-primary btn-xl"
                    id="registerButton "
                  >
                    Register
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  );
}
