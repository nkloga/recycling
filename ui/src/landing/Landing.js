import React from "react";
import Nav from "../components/Nav";
import Top from "../components/Top";
import About from "../components/About";
import Login from "../components/Login.js";
import Registration from "../components/Registration";
import Footer from "../components/Footer";

export default function Landing() {
  return (
    <div>
      <Nav />
      <Top />
      <Login />
      <About />
      <Registration />
      <Footer />
    </div>
  );
}
