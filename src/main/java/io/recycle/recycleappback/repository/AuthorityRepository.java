package io.recycle.recycleappback.repository;

import io.recycle.recycleappback.model.Authority;
import io.recycle.recycleappback.model.enums.AuthorityName;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorityRepository extends CrudRepository<Authority, Long> {
    @Query("FROM Authority WHERE authority=:authority")
    Authority findByName(@Param("authority") AuthorityName authority);
}