package io.recycle.recycleappback.repository;

import io.recycle.recycleappback.model.DBFile;
import io.recycle.recycleappback.model.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRepository extends CrudRepository<Question, Long> {

}
