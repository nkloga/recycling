package io.recycle.recycleappback.repository;

import io.recycle.recycleappback.model.Answer;
import io.recycle.recycleappback.model.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnswerRepository extends CrudRepository<Answer, Long> {

    @Query("FROM Answer WHERE game_id=:id")
    List<Answer> getGameStats(@Param("id") Long id);
}
