package io.recycle.recycleappback.security;

import io.recycle.recycleappback.model.Authority;
import io.recycle.recycleappback.model.User;
import io.recycle.recycleappback.model.dto.GameDto;
import io.recycle.recycleappback.model.dto.UserDto;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Factory class to convert user into DTO object and set authorities
 */
public final class JwtUserFactory {

    private JwtUserFactory() {
    }

    public static UserDto create(User user) {

        return new UserDto(user.getId(),
                user.getUsername(),
                user.getEmail(),
                user.getPassword(),
                mapToGrantedAuthorities(user.getAuthorities()),
                user.getEnabled(),
                user.getLastPasswordResetDate());
    }

    public static List<GrantedAuthority> mapToGrantedAuthorities(List<Authority> authorities) {
        return authorities.stream().map(authority -> new SimpleGrantedAuthority(authority.getAuthority().name())).collect(Collectors.toList());
    }
}
