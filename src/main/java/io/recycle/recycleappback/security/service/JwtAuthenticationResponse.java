package io.recycle.recycleappback.security.service;

import java.io.Serializable;

public class JwtAuthenticationResponse implements Serializable {

    private static final long serialVersionUID = 1250166508152483573L;

    private final String token;

    private final String auth;

    public JwtAuthenticationResponse(String token, String auth) {
        this.token = token;
        this.auth = auth;
    }

    public String getToken() {
        return this.token;
    }

    public String getAuth() {
        return auth;
    }
}
