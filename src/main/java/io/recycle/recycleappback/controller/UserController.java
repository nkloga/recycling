package io.recycle.recycleappback.controller;

import io.recycle.recycleappback.model.User;
import io.recycle.recycleappback.model.dto.GameDto;
import io.recycle.recycleappback.model.dto.UserDto;
import io.recycle.recycleappback.service.UserService;
import io.recycle.recycleappback.security.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;

@RequestMapping("/user")
@RestController
public class UserController {

    private final UserService userService;

    @Value("${jwt.header}")
    private String tokenHeader;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    @Qualifier("jwtUserDetailsService")
    private UserDetailsService userDetailsService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Run after succesful login to get current user profile info
     *
     * @return currently logged as dto object
     */
    @GetMapping(value = "")
    public UserDto getAuthenticatedUser(HttpServletRequest request) {
        String token = request.getHeader(tokenHeader).substring(7);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        UserDto user = (UserDto) userDetailsService.loadUserByUsername(username);
        return user;
    }

    /**
     * Registration for passenger or driver
     *
     * @param user object with sensitive info (pw, credit card info etc)
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> registerUser(@RequestBody User user) {
        return ResponseEntity.ok(userService.register(user));
    }
}