package io.recycle.recycleappback.controller;

import io.recycle.recycleappback.model.dto.AnswerDto;
import io.recycle.recycleappback.model.dto.GameDto;
import io.recycle.recycleappback.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/game")
@RestController
public class GameController {

    private GameService gameService;

    @Autowired
    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GameDto> createGame() {
        return ResponseEntity.ok(gameService.create());
    }

    @RequestMapping(value = "/start/{id}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GameDto> startGame(@PathVariable Long id) {
        return ResponseEntity.ok(gameService.startGame(id));
    }

    @RequestMapping(value = "/join/{id}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GameDto> joinGame(@PathVariable Long id) {
        return ResponseEntity.ok(gameService.joinGame(id));
    }

    @RequestMapping(value = "/add-results/{id}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<AnswerDto>> addResults(@RequestBody List<AnswerDto> answers) {
        return ResponseEntity.ok(gameService.addGameResults(answers));
    }

    @RequestMapping(value = "/stats/{id}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<AnswerDto>> getStats(@PathVariable Long id) {
        return ResponseEntity.ok(gameService.getGameStats(id));
    }


//
//    @GetMapping("/downloadDriverPhoto")
//    public UploadFileResponse downloadProfilePhoto() throws Exception {
//        UploadFileResponse response = new UploadFileResponse(null, null, null, 0L);
//        logger.info("GET Load file from database");
//        Driver driver = userService.getCurrentUser().getDriver();
//        if (driver != null) {
//            if (driver.getDriversPhoto() != null) {
//                DBFile dbFile = DBFileStorageService.getFile(driver.getDriversPhoto().getId());
//                String fileDownloadUri = "/user/downloadDriverPhoto/" + dbFile.getId();
//                UploadFileResponse uploadFileResponse = new UploadFileResponse(dbFile.getFileName(), fileDownloadUri, null, 0L);
//                logger.info("Got response: " + uploadFileResponse);
//                return uploadFileResponse;
//            }
//            return response;
//        } else return response;
//    }
}