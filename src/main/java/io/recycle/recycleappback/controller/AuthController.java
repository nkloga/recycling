package io.recycle.recycleappback.controller;


import io.recycle.recycleappback.exception.AuthenticationException;
import io.recycle.recycleappback.model.User;
import io.recycle.recycleappback.model.dto.GameDto;
import io.recycle.recycleappback.model.dto.UserDto;
import io.recycle.recycleappback.service.UserService;
import io.recycle.recycleappback.security.JwtAuthenticationRequest;
import io.recycle.recycleappback.security.JwtTokenUtil;
import io.recycle.recycleappback.security.JwtUserFactory;
import io.recycle.recycleappback.security.service.JwtAuthenticationResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@RestController
public class AuthController {

    private final Logger logger = LoggerFactory.getLogger(AuthController.class);

    @Value("${jwt.header}")
    private String tokenHeader;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserService userService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    @Qualifier("jwtUserDetailsService")
    private UserDetailsService userDetailsService;

    /**
     * @param authenticationRequest consist from username and password
     * @return bearer token
     */

    @CrossOrigin(origins = "http://localhost:3000")

    @RequestMapping(value = "${jwt.route.authentication.path}", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtAuthenticationRequest authenticationRequest) {
        logger.info("POST request with body: " + authenticationRequest + " creating authentication token");
        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

        // Reload password post-security so we can generate the token
        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        User user =  userService.findByUserName(authenticationRequest.getUsername());
        UserDto userDto = JwtUserFactory.create(user);

        final String token = jwtTokenUtil.generateToken(userDto);
        final String auth = userDetails.getAuthorities().stream().findFirst().get().getAuthority();
        logger.info("Got token: " + token);
        // Return the token
        return ResponseEntity.ok(new JwtAuthenticationResponse(token, auth));
    }

    /**
     * @param request
     * @return updated bearer token
     */
    @RequestMapping(value = "${jwt.route.authentication.refresh}", method = RequestMethod.GET)
    public ResponseEntity<?> refreshAndGetAuthenticationToken(HttpServletRequest request) {
        logger.info("GET request to refresh token");
        String authToken = request.getHeader(tokenHeader);
        final String token = authToken.substring(7);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        UserDto user = (UserDto) userDetailsService.loadUserByUsername(username);

        if (jwtTokenUtil.canTokenBeRefreshed(token, user.getLastPasswordResetDate())) {
            String refreshedToken = jwtTokenUtil.refreshToken(token);
            logger.info("Token can be refreshed: " + refreshedToken);
            final String auth = user.getAuthorities().stream().findFirst().get().getAuthority();
            return ResponseEntity.ok(new JwtAuthenticationResponse(refreshedToken,auth));
        } else {
            return ResponseEntity.badRequest().body(null);
        }
    }

    @ExceptionHandler({AuthenticationException.class})
    public ResponseEntity<String> handleAuthenticationException(AuthenticationException e) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
    }

    /**
     * Authenticates the user. If something is wrong, an {@link AuthenticationException} will be thrown
     */
    private void authenticate(String username, String password) {
        logger.info("Authenticating user: " + username);
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
            logger.info("Successfully authenticated");
        } catch (DisabledException e) {
            throw new AuthenticationException("User is disabled", e);
        } catch (BadCredentialsException e) {
            throw new AuthenticationException("Bad credentials", e);
        }
    }
}
