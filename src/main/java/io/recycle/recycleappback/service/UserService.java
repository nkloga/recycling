package io.recycle.recycleappback.service;


import io.recycle.recycleappback.model.User;
import io.recycle.recycleappback.model.dto.GameDto;
import io.recycle.recycleappback.model.dto.UserDto;

public interface UserService {

    User findByUserName(String username);

    /**
     * Used to create user account
     *
     * @param user Profile data provided by the user, including password, therefore standard UserDto was not used
     * @return saved user model if registration was successful
     */
    UserDto register(User user);

    User authenticate();

}
