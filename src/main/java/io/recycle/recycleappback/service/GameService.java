package io.recycle.recycleappback.service;

import io.recycle.recycleappback.model.Answer;
import io.recycle.recycleappback.model.Game;
import io.recycle.recycleappback.model.Question;
import io.recycle.recycleappback.model.dto.AnswerDto;
import io.recycle.recycleappback.model.dto.GameDto;

import java.util.List;

public interface GameService {
    GameDto create();

    GameDto startGame(Long id);

    GameDto joinGame(Long id);

    List<AnswerDto> addGameResults(List<AnswerDto> answers);

    List<AnswerDto> getGameStats(Long id);

    Game getGameById(Long id);

    Question getQuestionById(Long id);
}
