package io.recycle.recycleappback.service;

import io.recycle.recycleappback.model.dto.AuthorityDto;
import io.recycle.recycleappback.model.enums.AuthorityName;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

public interface AuthorityService {

    /**
     * Create authority, allowed PASSENGER, DRIVER or ADMIN
     *
     * @return authorityDto object
     */
    AuthorityDto add(SimpleGrantedAuthority authorityDto);

    /**
     * Get authority by id
     *
     * @param id User id
     * @return user authorityDto object
     */
    AuthorityDto getAuthorityById(Long id);
    /**
     * Get authority by name
     *
     * @param name AuthorityName, allowed PASSENGER, DRIVER or ADMIN
     * @return user authorityDto object
     */
    AuthorityDto getAuthorityByName(AuthorityName name);
}
