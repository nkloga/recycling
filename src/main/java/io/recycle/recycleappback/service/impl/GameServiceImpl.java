package io.recycle.recycleappback.service.impl;

import io.recycle.recycleappback.model.Answer;
import io.recycle.recycleappback.model.Game;
import io.recycle.recycleappback.model.Question;
import io.recycle.recycleappback.model.User;
import io.recycle.recycleappback.model.dto.AnswerDto;
import io.recycle.recycleappback.model.dto.GameDto;
import io.recycle.recycleappback.model.enums.GameStatus;
import io.recycle.recycleappback.repository.AnswerRepository;
import io.recycle.recycleappback.repository.GameRepository;
import io.recycle.recycleappback.repository.QuestionRepository;
import io.recycle.recycleappback.service.GameService;
import io.recycle.recycleappback.service.UserService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class GameServiceImpl implements GameService {

    private GameRepository gameRepository;
    private AnswerRepository answerRepository;
    private UserService userService;
    private final Logger logger = LoggerFactory.getLogger(GameServiceImpl.class);
    private final ModelMapper modelMapper;
    private QuestionRepository questionRepository;


    @Autowired
    public GameServiceImpl(GameRepository gameRepository, AnswerRepository answerRepository, UserService userService, ModelMapper modelMapper, QuestionRepository questionRepository) {
        this.gameRepository = gameRepository;
        this.answerRepository = answerRepository;
        this.userService = userService;
        this.modelMapper = modelMapper;
        this.questionRepository = questionRepository;
    }

    public GameDto create() {
        Game game = new Game();
        game.setGameStatus(GameStatus.CREATED);
        return modelMapper.map(gameRepository.save(game), GameDto.class);
    }

    @Override
    public GameDto startGame(Long id) {
        GameDto gameDto = null;
        Game game = getGameById(id);
        if (game != null) {
            game.setGameStatus(GameStatus.ONGOING);
            gameDto = modelMapper.map(gameRepository.save(game), GameDto.class);
        }
        return gameDto;
    }

    @Override
    public GameDto joinGame(Long id) {
        GameDto gameDto = null;
        Game game = getGameById(id);
        if (game != null) {
            List<User> participants = game.getUsers();
            participants.add(userService.authenticate());
            game.setUsers(participants);
            gameDto = modelMapper.map(gameRepository.save(game), GameDto.class);
        }
        return gameDto;
    }

    @Override
    public List<AnswerDto> addGameResults(List<AnswerDto> answers) {
        answers.forEach(a -> {
            Answer answer = new Answer();
            answer.setAnswer(a.getAnswer());
            answer.setGame(getGameById(a.getGame()));
            answer.setId(a.getId());
            answer.setQuestion(getQuestionById(a.getQuestion()));
            answer.setUsername(a.getUsername());
            answerRepository.save(answer);
        });
        return answers;
    }

    @Override
    public List<AnswerDto> getGameStats(Long id) {
        List<AnswerDto> answers = null;
        try {
            answers = answerRepository.getGameStats(id).stream().map(answer -> {
                AnswerDto answerDto = new AnswerDto();
                answerDto.setAnswer(answer.getAnswer());
                answerDto.setGame(answer.getGame().getId());
                answerDto.setQuestion(answer.getQuestion().getId());
                answerDto.setId(answer.getId());
                answerDto.setUsername(answer.getUsername());
                return answerDto;
            }).collect(Collectors.toList());
        } catch (Exception e) {
            logger.error("Cannot fetch game results with id " + id);
        }
        return answers;
    }

    @Override
    public Game getGameById(Long id) {
        Game game = null;
        try {
            game = gameRepository.findById(id).orElseThrow(Exception::new);
        } catch (Exception e) {
            logger.error("Cannot get a game with the id " + id + ", error: " + e);
        }
        return game;
    }

    @Override
    public Question getQuestionById(Long id) {
        Question game = null;
        try {
            game = questionRepository.findById(id).orElseThrow(Exception::new);
        } catch (Exception e) {
            logger.error("Cannot get a question with the id " + id + ", error: " + e);
        }
        return game;
    }

}
