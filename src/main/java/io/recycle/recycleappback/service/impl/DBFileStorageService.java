package io.recycle.recycleappback.service.impl;

import io.recycle.recycleappback.model.DBFile;
import io.recycle.recycleappback.repository.DBFileRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.Objects;

/**
 * Service class to handle user profile picture files
 */
@Service
public class DBFileStorageService {

    private final Logger logger = LoggerFactory.getLogger(DBFileStorageService.class);

    @Autowired
    private DBFileRepository dbFileRepository;


    public DBFile storeFile(MultipartFile file) {
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        logger.info("Normalized file name: " + fileName);
        DBFile dbFile = null;
        try {
            // Check if the file's name contains invalid characters
            if (fileName.contains("..")) {
                throw new Exception("Invalid char");
            }
            logger.info("File doesn't contain invalid characters");

            dbFile = new DBFile(fileName, file.getContentType(), file.getBytes());
            logger.info("Made a DBFile instance: " + dbFile);

            dbFile = dbFileRepository.save(dbFile);
        } catch (Exception ex) {
        }
        return dbFile;
    }


    public DBFile getFile(String fileId) {
        logger.info("Getting file by id: " + fileId);
        DBFile dbFile = null;
        try {
            dbFile = dbFileRepository.findById(fileId).orElseThrow(() -> new Exception("FILE_NOT_FOUND"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dbFile;
    }
}
