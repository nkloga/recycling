package io.recycle.recycleappback.service.impl;


import io.recycle.recycleappback.model.Authority;
import io.recycle.recycleappback.model.User;
import io.recycle.recycleappback.model.dto.GameDto;
import io.recycle.recycleappback.model.dto.UserDto;
import io.recycle.recycleappback.repository.UserRepository;
import io.recycle.recycleappback.security.JwtUserFactory;
import io.recycle.recycleappback.service.AuthorityService;
import io.recycle.recycleappback.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service class implementing all user related methods
 */

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
    private AuthorityService authorityService;
    private final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);


    @Autowired
    public UserServiceImpl(UserRepository userRepository, AuthorityService authorityService) {
        this.userRepository = userRepository;
        this.authorityService = authorityService;
    }


    @Override
    public User authenticate() {
    User user = null;
        try {
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            user = userRepository.findByUsername(username);
        } catch (NullPointerException exc) {
            logger.error("Cannot get security context data: " + exc);
        }
        return user;
    }

    @Override
    public UserDto register(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        List<Authority> authorities = user.getAuthorities().stream().map(auth -> authorityService.add(new SimpleGrantedAuthority(auth.getAuthority().name()))).map(auth -> {
            Authority authority = new Authority();
            authority.setAuthority(auth.getAuthority());
            authority.setId(auth.getId());
            return authority;
        }).collect(Collectors.toList());
        user.setAuthorities(authorities);
        UserDto userDto =null;
        if (userRepository.findByUsername(user.getUsername()) == null) {
            if (userRepository.findByEmail(user.getEmail()) == null) {
                user.setEnabled(true);
                User savedUser = null;
                try {
                    savedUser = userRepository.save(user);
                } catch (Exception ignored) {
                }
                userDto = new UserDto(savedUser.getId(), savedUser.getUsername(), savedUser.getEmail(), savedUser.getPassword(), JwtUserFactory.mapToGrantedAuthorities(savedUser.getAuthorities()), savedUser.getEnabled(), savedUser.getLastPasswordResetDate());
            }
        }
        return userDto;
    }

    @Override
    public User findByUserName(String username) {
        return userRepository.findByUsername(username);
    }


}
