package io.recycle.recycleappback.model;


import io.recycle.recycleappback.model.enums.GameStatus;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "GAME")
public class Game {

    @Id
    @GeneratedValue(generator = "sequence-generator")
    @GenericGenerator(name = "sequence-generator", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {@Parameter(name = "sequence_name", value = "user_sequence"), @Parameter(name = "initial_value", value = "1000"), @Parameter(name = "increment_size", value = "1")})
    private Long id;

    @Column(name = "GAME_STATUS")
    @Enumerated
//    @NotNull
    private GameStatus gameStatus;

    @ManyToMany
    @JoinTable(name = "GAME_USER", joinColumns = {@JoinColumn(name = "GAME_ID", referencedColumnName = "ID")}, inverseJoinColumns = {@JoinColumn(name = "USER_ID", referencedColumnName = "ID")})
    private List<User> users;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "GAME_QUESTION", joinColumns = {@JoinColumn(name = "GAME_ID", referencedColumnName = "ID")}, inverseJoinColumns = {@JoinColumn(name = "QUESTION_ID", referencedColumnName = "ID")})
    private List<Question> gameQuestions;

    public Game(Long id
            , GameStatus gameStatus
            ,
                List<User> users,
                List<Question> gameQuestions
    ) {
        this.id = id;
        this.gameStatus = gameStatus;
        this.users = users;
        this.gameQuestions = gameQuestions;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public GameStatus getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(GameStatus gameStatus) {
        this.gameStatus = gameStatus;
    }

    public Game() {
    }


    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<Question> getGameQuestions() {
        return gameQuestions;
    }

    public void setGameQuestions(List<Question> gameQuestions) {
        this.gameQuestions = gameQuestions;
    }
}
