package io.recycle.recycleappback.model.dto;


import com.fasterxml.jackson.annotation.JsonIgnore;
import io.recycle.recycleappback.model.enums.GameStatus;
import io.recycle.recycleappback.model.Question;
import io.recycle.recycleappback.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Date;
import java.util.List;


public class GameDto {

    private Long id;
    private GameStatus gameStatus;
    private List<String> users;
    private List<QuestionDto> gameQuestions;

    public GameDto(Long id, GameStatus gameStatus, List<String> users, List<QuestionDto> gameQuestions) {
        this.id = id;
        this.gameStatus = gameStatus;
        this.users = users;
        this.gameQuestions = gameQuestions;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public GameStatus getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(GameStatus gameStatus) {
        this.gameStatus = gameStatus;
    }

    public GameDto() {
    }

    public List<String> getUsers() {
        return users;
    }

    public void setUsers(List<String> users) {
        this.users = users;
    }

    public List<QuestionDto> getGameQuestions() {
        return gameQuestions;
    }

    public void setGameQuestions(List<QuestionDto> gameQuestions) {
        this.gameQuestions = gameQuestions;
    }


}
