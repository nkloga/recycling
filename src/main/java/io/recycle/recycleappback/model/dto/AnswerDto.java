package io.recycle.recycleappback.model.dto;


import io.recycle.recycleappback.model.Game;
import io.recycle.recycleappback.model.Question;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


public class AnswerDto {

    private Long id;
    private Long game;
    private Long question;
    private Integer answer;
    private String username;

    public AnswerDto() {
    }

    public AnswerDto(Long id, Long game, Long question, Integer answer, String username) {
        this.id = id;
        this.game = game;
        this.question = question;
        this.answer = answer;
        this.username = username;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getGame() {
        return game;
    }

    public void setGame(Long game) {
        this.game = game;
    }

    public Long getQuestion() {
        return question;
    }

    public void setQuestion(Long question) {
        this.question = question;
    }

    public Integer getAnswer() {
        return answer;
    }

    public void setAnswer(Integer answer) {
        this.answer = answer;
    }
}
