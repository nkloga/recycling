package io.recycle.recycleappback.model.dto;


import io.recycle.recycleappback.model.DBFile;
import io.recycle.recycleappback.model.Game;
import java.util.List;


public class QuestionDto {

    private Long id;
    private String question;
    private DBFileDto img;
    private String comment;

    public QuestionDto() {
    }

    public QuestionDto(Long id, String question, DBFileDto img, String comment) {
        this.id = id;
        this.question = question;
        this.img = img;
        this.comment = comment;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public DBFileDto getImg() {
        return img;
    }

    public void setImg(DBFileDto img) {
        this.img = img;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}