package io.recycle.recycleappback.model;


import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "ANSWER")
public class Answer {

    @Id
    @GeneratedValue(generator = "sequence-generator")
    @GenericGenerator(name = "sequence-generator", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {@Parameter(name = "sequence_name", value = "user_sequence"), @Parameter(name = "initial_value", value = "1000"), @Parameter(name = "increment_size", value = "1")})
    private Long id;


    @ManyToOne(targetEntity = Game.class)
    private Game game;

    @OneToOne(targetEntity = Question.class)
    private Question question;

    @Column(name = "ANSWER")
    @NotNull
    private Integer answer;

    @Column(name = "USERNAME")
    @NotNull
    private String username;

    public Answer() {
    }

    public Answer(Long id, Game game, Question question, @NotNull Integer answer, @NotNull String username) {
        this.id = id;
        this.game = game;
        this.question = question;
        this.answer = answer;
        this.username = username;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Integer getAnswer() {
        return answer;
    }

    public void setAnswer(Integer answer) {
        this.answer = answer;
    }
}
