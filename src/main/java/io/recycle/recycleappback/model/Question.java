package io.recycle.recycleappback.model;


import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "QUESTION")
public class Question {

    @Id
    @GeneratedValue(generator = "sequence-generator")
    @GenericGenerator(name = "sequence-generator", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {@Parameter(name = "sequence_name", value = "user_sequence"), @Parameter(name = "initial_value", value = "1000"), @Parameter(name = "increment_size", value = "1")})
    private Long id;

    @Column(name = "QUESTION", length = 50)
    @NotNull
    @Size(min = 4, max = 50)
    private String question;

    @OneToOne(targetEntity = DBFile.class)
    private DBFile img;


    @Column(name = "COMMENT", length = 1000)
    @Size(min = 6, max = 1000)
    private String comment;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            }, mappedBy = "gameQuestions")
    private List<Game> gameQuestions;

    public Question(Long id, @NotNull @Size(min = 4, max = 50) String question, DBFile img, @Size(min = 6, max = 1000) String comment
            , List<Game> gameQuestions
    ) {
        this.id = id;
        this.question = question;
        this.img = img;
        this.comment = comment;
        this.gameQuestions = gameQuestions;
    }

    public Question() {
    }

    public DBFile getImg() {
        return img;
    }

    public void setImg(DBFile img) {
        this.img = img;
    }

    public Long getId() {
        return id;
    }

    public List<Game> getGameQuestions() {
        return gameQuestions;
    }

    public void setGameQuestions(List<Game> gameQuestions) {
        this.gameQuestions = gameQuestions;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
