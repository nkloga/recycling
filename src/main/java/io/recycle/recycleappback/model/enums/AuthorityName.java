package io.recycle.recycleappback.model.enums;

public enum AuthorityName {
    TEACHER, STUDENT
}