package io.recycle.recycleappback.model.enums;

public enum GameStatus {
    CREATED, ONGOING, FINISHED
}