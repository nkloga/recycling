MERGE INTO AUTHORITY KEY (id) VALUES (26,'STUDENT');
MERGE INTO AUTHORITY KEY (id) VALUES (25,'TEACHER');

INSERT INTO USER (id, email, enabled, password, username)
VALUES(101, 'asda@test8.ee', true,  '$2a$10$kxZWk9srZJUusSgJRyiiy.lMg3iTIJ9b53SvNOoG.bmCHL9.KCsDy', 'teacher');

INSERT INTO USER (id, email, enabled, password, username)
VALUES(102, 'stud@test8.ee', true,  '$2a$10$kxZWk9srZJUusSgJRyiiy.lMg3iTIJ9b53SvNOoG.bmCHL9.KCsDy', 'student');

INSERT INTO USER (id, email, enabled, password, username)
VALUES(103, 'stud2@test8.ee', true,  '$2a$10$kxZWk9srZJUusSgJRyiiy.lMg3iTIJ9b53SvNOoG.bmCHL9.KCsDy', 'student1');

INSERT INTO USER (id, email, enabled, password, username)
VALUES(104, 'stud3@test8.ee', true,  '$2a$10$kxZWk9srZJUusSgJRyiiy.lMg3iTIJ9b53SvNOoG.bmCHL9.KCsDy', 'student2');


INSERT INTO USER_AUTHORITY (user_id, authority_id) VALUES (102,26);
INSERT INTO USER_AUTHORITY (user_id, authority_id) VALUES (103,26);
INSERT INTO USER_AUTHORITY (user_id, authority_id) VALUES (104,26);
INSERT INTO USER_AUTHORITY (user_id, authority_id) VALUES (101,25);

INSERT INTO GAME (id, game_status) VALUES (200,0);
INSERT INTO GAME (id, game_status) VALUES (201,0);

INSERT INTO FILES (id, data, file_name, file_type) VALUES (400, null, 'filename', 'type');
INSERT INTO FILES (id, data, file_name, file_type) VALUES (401, null, 'filename', 'type');

INSERT INTO QUESTION (id, comment, question, img_id) VALUES (500, 'comment', 'question', 400);
INSERT INTO QUESTION (id, comment, question, img_id) VALUES (501, 'comment', 'question', 401);
INSERT INTO QUESTION (id, comment, question, img_id) VALUES (502, 'comment', 'question', 400);
INSERT INTO QUESTION (id, comment, question, img_id) VALUES (503, 'comment', 'question', 401);

insert into GAME_QUESTION (game_id, question_id) values (200,500);
insert into GAME_QUESTION (game_id, question_id) values (200,501);
insert into GAME_QUESTION (game_id, question_id) values (200,502);
insert into GAME_QUESTION (game_id, question_id) values (200,503);

insert into GAME_QUESTION (game_id, question_id) values (201,500);
insert into GAME_QUESTION (game_id, question_id) values (201,501);
insert into GAME_QUESTION (game_id, question_id) values (201,502);
insert into GAME_QUESTION (game_id, question_id) values (201,503);


